<?php
/**
 * @file
 * example_compare_configuration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function example_compare_configuration_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_entity_type_info().
 */
function example_compare_configuration_eck_entity_type_info() {
  $items = array(
    'question' => array(
      'name' => 'question',
      'label' => t('Question'),
      'properties' => 'a:4:{s:4:"uuid";i:1;s:3:"uid";i:1;s:7:"created";i:1;s:7:"changed";i:1;}',
      'custom_properties' => 'a:0:{}',
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function example_compare_configuration_node_info() {
  $items = array(
    'answer' => array(
      'name' => t('Answer'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
