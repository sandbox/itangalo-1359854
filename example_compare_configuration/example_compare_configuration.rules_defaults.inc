<?php
/**
 * @file
 * example_compare_configuration.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function example_compare_configuration_default_rules_configuration() {
  $items = array();
  $items['rules_evaluate_answer'] = entity_import('rules_config', '{ "rules_evaluate_answer" : {
      "LABEL" : "Evaluate answer",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "compare" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_answer_question" } },
        { "entity_has_field" : {
            "entity" : [ "node:field-answer-question" ],
            "field" : "field_evaluation_component"
          }
        },
        { "compare_condition_dynamic_condition" : {
            "component" : [ "node:field-answer-question:field-evaluation-component" ],
            "question" : [ "node:field-answer-question" ],
            "answer" : [ "node" ]
          }
        }
      ],
      "DO" : [ { "drupal_message" : { "message" : "It is true." } } ]
    }
  }');
  $items['rules_is_equal_to'] = entity_import('rules_config', '{ "rules_is_equal_to" : {
      "LABEL" : "Is equal to",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "question" : { "label" : "Question", "type" : "question" },
        "answer" : { "label" : "Answer", "type" : "node" }
      },
      "AND" : [
        { "entity_has_field" : { "entity" : [ "answer" ], "field" : "field_answer_answer" } },
        { "entity_has_field" : { "entity" : [ "question" ], "field" : "field_correct_answer" } },
        { "data_is" : {
            "data" : [ "answer:field-answer-answer" ],
            "value" : [ "question:field-correct-answer" ]
          }
        }
      ]
    }
  }');
  $items['rules_is_one_of'] = entity_import('rules_config', '{ "rules_is_one_of" : {
      "LABEL" : "Is one of",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "question" : { "label" : "Question", "type" : "question" },
        "answer" : { "label" : "Answer", "type" : "node" }
      },
      "AND" : [
        { "entity_has_field" : { "entity" : [ "question" ], "field" : "field_multiple_answers" } },
        { "entity_has_field" : { "entity" : [ "answer" ], "field" : "field_answer_answer" } },
        { "list_contains" : {
            "list" : [ "question:field-multiple-answers" ],
            "item" : [ "answer:field-answer-answer" ]
          }
        }
      ]
    }
  }');
  return $items;
}
