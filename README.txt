This project is a first small step in an attempt to do something similar to
what is found at the Khan Academy (khanacademy.org).

This first step allows for setting up Rules components used for evaluating an
answer towards a given correct answer, in dynamic ways. It contains:

* A new entity type, "Question". It is used to store questions, and descriptions
  of answers (either in exact form or in lists of answer alternatives). It also
  has a reference field to Rules components, for allowing the question creator
  to select which kind of logic should be used for evaluating answers.
* Two Rules condition components, "is equal to" and "is one of". These are used
  by question creators to select answer evaluation logic.
* A content type, "Answer". This contains a reference to a question, and a field
  for answer.
* A reaction rule acting when an answer is saved, using the component selected
  in the question to evaluate the answer (against the question data). If the
  conditions are met, the text 'it is true' is displayed.

The idea is that the question entity should be expanded with a few more fields,
to allow more flexibility in how accepted answers can be set. These fields
should be visible/hidden depending on which evaluation components are used.

More importantly, though, there should be many more evaluation components – for
example to evaluate mathematical expressions, check for *all* words in a list,
check for similar words, and other things you could set up using Rules
conditions.

Some links for trying this out:
* Create a question: admin/structure/eck/question/question/add
* Create an answer: node/add/answer
* Change fields for the questions: admin/structure/eck/question/question/fields

IDEAS FOR A DISTANT FUTURE
==========================

* Questions should be able to use token replacement patterns in the question
  texts, allowing for dynamic questions following specified patterns.
* There should probably be a separate entity type for answers. (Which actually
  makes more sense than a separate question type for answers.) Answers need to
  be light-weight, and should probably be archived or deleted after a certain
  time.
* There should be rules and stuff for setting up achievements and rewards.
* There should be permissions associated with each answer evaluation component,
  allowing access to more sophisticated evaluators as the user account grows.
* There should be a separate component type in Rules for answer evaluations,
  which would also make it easier to only allow certain type of components to be
  selected as answer evaluations.
* And more...
