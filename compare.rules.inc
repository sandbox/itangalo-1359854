<?php
/**
 * @file
 * Provides a dynamic condition, allowing for using different Rules components
 * when evaluating a condition.
 */

/**
 * Implements hook_rules_condition_info().
 */
function compare_rules_condition_info() {
  $conditions = array(
    'compare_condition_dynamic_condition' => array(
      'label' => t('Evaluate a condition component'),
      'group' => t('Compare'),
      'parameter' => array(
        'component' => array(
          'type' => 'rules_config',
          'label' => t('Condition component to check'),
          // @TODO: Limit the selectable components to conditions of a type
          // created for this comparison purpose.
          'restriction' => 'selector',
        ),
        'question' => array(
          'type' => 'entity',
          'label' => t('Question entity'),
        ),
        'answer' => array(
          'type' => 'entity',
          'label' => t('Answer entity'),
        ),
      ),
    ),
  );

  return $conditions;
}

/**
 * The 'compare_condition_dynamic_condition' evaluation callback.
 */
function compare_condition_dynamic_condition($component, $question, $answer) {
  $result = rules_invoke_component($component->name, $question, $answer);
  // Check that the component actually returns TRUE/FALSE. This is not always
  // the case, since the user is allowed to select non-condition components.
  if (is_bool($result)) {
    return $result;
  }

  return FALSE;
}
